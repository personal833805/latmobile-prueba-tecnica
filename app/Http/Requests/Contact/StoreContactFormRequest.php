<?php

namespace App\Http\Requests\Contact;

use Illuminate\Foundation\Http\FormRequest;

class StoreContactFormRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'first_name.required' => 'The first name is required',
            'first_name.string' => 'The first name must be a text',
            'last_name.required' => 'The last name is required',
            'last_name.string' => 'The last name must be a text',
            'phone.required' => 'The phone is required',
        ];
    }
}
