<?php

namespace App\Http\Controllers\Contacts;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\StoreContactFormRequest;
use App\Models\Contact;

class StoreContactController extends Controller
{
    public function __invoke(StoreContactFormRequest $request)
    {
        $request->validated();
        Contact::create($request->post());

        return redirect()->route('contacts')->with('success', 'Contact has been created');
    }
}
