<?php

namespace App\Http\Controllers\Contacts;

use App\Http\Controllers\Controller;
use App\Models\Contact;

class GetContactsController extends Controller
{
    public function __invoke()
    {
        $contacts = Contact::orderBy('first_name')->paginate(3);
        return view('contacts/index', compact('contacts'));
    }
}
