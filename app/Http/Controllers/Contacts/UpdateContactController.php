<?php

namespace App\Http\Controllers\Contacts;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\UpdateContactFormRequest;
use App\Models\Contact;

class UpdateContactController extends Controller
{
    public function __invoke(UpdateContactFormRequest $request, Contact $contact)
    {
        $request->validated();

        $contact->fill($request->post())->save();
        return redirect()->route('contacts')->with('success', 'Contact has been updated');
    }
}
