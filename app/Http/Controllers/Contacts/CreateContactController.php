<?php

namespace App\Http\Controllers\Contacts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CreateContactController extends Controller
{
    public function __invoke()
    {
        return view('contacts.create');
    }
}
