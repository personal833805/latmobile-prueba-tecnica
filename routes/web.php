<?php

use App\Http\Controllers\Contacts\CreateContactController;
use App\Http\Controllers\Contacts\DeleteContactController;
use App\Http\Controllers\Contacts\GetContactsController;
use App\Http\Controllers\Contacts\ShowContactController;
use App\Http\Controllers\Contacts\StoreContactController;
use App\Http\Controllers\Contacts\UpdateContactController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::prefix('/contacts')->group(function () {
        Route::get('/', GetContactsController::class)->name('contacts');
        Route::get('/create', CreateContactController::class)->name('contact.create');
        Route::post('/', StoreContactController::class)->name('contact.store');
        Route::get('/{contact}', ShowContactController::class)->name('contact.show');
        Route::put('/{contact}', UpdateContactController::class)->name('contact.update');
        Route::delete('/{contact}', DeleteContactController::class)->name('contact.delete');
    });
});
