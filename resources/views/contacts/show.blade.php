<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Edit Contact') }}
            </h2>
            <a href="{{ route('contacts') }}" class="bg-transparent hover:bg-gray-100 text-green-600 font-semibold hover:text-white py-2 px-4 outline outline-1 border border-green hover:border-transparent rounded">
                Go Back
            </a>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-6 py-8">
                <form action="{{ route('contact.update', $contact) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="mb-6">
                        <label for="first_name" class="text-sm font-medium text-gray-900 block mb-2">First Name</label>
                        <input type="text" id="first_name" name="first_name" value="{{ $contact->first_name }}" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full p-2.5" required>
                        @error('first_name')
                        <div class="text-red-500 mt-1">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-6">
                        <label for="last_name" class="text-sm font-medium text-gray-900 block mb-2">Last Name</label>
                        <input type="text" id="last_name" name="last_name" value="{{ $contact->last_name }}" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full p-2.5" required>
                        @error('last_name')
                        <div class="text-red-500 mt-1">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mb-6">
                        <label for="phone" class="text-sm font-medium text-gray-900 block mb-2">Phone</label>
                        <input type="text" id="phone" name="phone" value="{{ $contact->phone  }}" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full p-2.5" required>
                        @error('phone')
                        <div class="text-red-500 mt-1">{{ $message }}</div>
                        @enderror
                    </div>
                    <button type="submit" class="bg-green-600 text-white text-sm px-4 py-2 rounded">Submit</button>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
