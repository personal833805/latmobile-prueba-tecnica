<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Contacts') }}
            </h2>
            <a href="{{ route('contact.create') }}" class="font-medium text-white bg-green-600 px-3 py-2 rounded">
                Create contact
            </a>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg my-2">
                @if ($message = Session::get('success'))
                    <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative ml-4 mr-4 my-4" role="alert">
                        <strong class="font-bold">Success!</strong>
                        <span class="block sm:inline">{{ $message }}.</span>
                    </div>
                @endif
                <table class="w-full text-sm text-left">
                    <thead class="text-xs uppercase bg-gray-50">
                    <tr>
                        <th class="px-6 py-3">First name</th>
                        <th class="px-6 py-3">Last name</th>
                        <th class="px-6 py-3">Phone</th>
                        <th class="px-6 py-3">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($contacts as $contact)
                            <tr class="bg-white border-b hover:bg-gray-100">
                                <td class="px-6 py-4">{{ $contact->first_name }}</td>
                                <td class="px-6 py-4">{{ $contact->last_name }}</td>
                                <td class="px-6 py-4">{{ $contact->phone  }}</td>
                                <td class="px-6 py-4">
                                    <a href="{{ route('contact.show', $contact)  }}" class="font-medium text-blue-600 hover:underline mr-2">Edit</a>
                                    <form action="{{ route('contact.delete', $contact->id) }}" method="POST" class="inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="font-medium text-red-600 hover:underline inline">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="px-3 py-2">
                    {{ $contacts->links() }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
